2019-09-05|Green Bay|10|Chicago|3
2019-09-08|Washington|27|Philadelphia|32
2019-09-08|Buffalo|17|NY Jets|16
2019-09-08|Atlanta|12|Minnesota|28
2019-09-08|Baltimore|59|Miami|10
2019-09-08|Kansas City|40|Jacksonville|26
2019-09-08|Tennessee|43|Cleveland|13
2019-09-08|LA Rams|30|Carolina|27
2019-09-08|San Francisco|31|Tampa Bay|17
2019-09-08|NY Giants|17|Dallas|35
2019-09-08|Detroit|27|Arizona|27
2019-09-08|Cincinnati|20|Seattle|21
2019-09-08|Indianapolis|24|LA Chargers|30
2019-09-08|Pittsburgh|3|New England|33
2019-09-09|Houston|28|New Orleans|30
2019-09-09|Denver|16|Oakland|24
2019-09-12|Tampa Bay|20|Carolina|14
2019-09-15|Arizona|17|Baltimore|23
2019-09-15|San Francisco|41|Cincinnati|17
2019-09-15|LA Chargers|10|Detroit|13
2019-09-15|Minnesota|16|Green Bay|21
2019-09-15|Jacksonville|12|Houston|13
2019-09-15|New England|43|Miami|0
2019-09-15|Buffalo|28|NY Giants|14
2019-09-15|Seattle|28|Pittsburgh|26
2019-09-15|Indianapolis|19|Tennessee|17
2019-09-15|Dallas|31|Washington|21
2019-09-15|Kansas City|28|Oakland|10
2019-09-15|Chicago|16|Denver|14
2019-09-15|New Orleans|9|LA Rams|27
2019-09-15|Philadelphia|20|Atlanta|24
2019-09-16|Cleveland|23|NY Jets|3
2019-09-19|Tennessee|7|Jacksonville|20
2019-09-22|Miami|6|Dallas|31
2019-09-22|Cincinnati|17|Buffalo|21
2019-09-22|Denver|16|Green Bay|27
2019-09-22|Atlanta|24|Indianapolis|27
2019-09-22|Baltimore|28|Kansas City|33
2019-09-22|Oakland|14|Minnesota|34
2019-09-22|NY Jets|14|New England|30
2019-09-22|Detroit|27|Philadelphia|24
2019-09-22|Carolina|38|Arizona|20
2019-09-22|NY Giants|32|Tampa Bay|31
2019-09-22|Houston|27|LA Chargers|20
2019-09-22|Pittsburgh|20|San Francisco|24
2019-09-22|New Orleans|33|Seattle|27
2019-09-22|LA Rams|20|Cleveland|13
2019-09-23|Chicago|31|Washington|15
2019-09-26|Philadelphia|34|Green Bay|27
2019-09-29|Jacksonville|26|Denver|24
2019-09-29|Minnesota|6|Chicago|16
2019-09-29|Tampa Bay|55|LA Rams|40
2019-09-29|Seattle|27|Arizona|10
2019-09-29|Washington|3|NY Giants|24
2019-09-29|LA Chargers|30|Miami|10
2019-09-29|Oakland|31|Indianapolis|24
2019-09-29|Carolina|16|Houston|10
2019-09-29|Kansas City|34|Detroit|30
2019-09-29|New England|16|Buffalo|10
2019-09-29|Cleveland|40|Baltimore|25
2019-09-29|Tennessee|24|Atlanta|10
2019-09-29|Dallas|10|New Orleans|12
2019-09-30|Cincinnati|3|Pittsburgh|27
2019-10-03|LA Rams|29|Seattle|30
2019-10-06|Green Bay|34|Dallas|24
2019-10-06|Jacksonville|27|Carolina|34
2019-10-06|Arizona|26|Cincinnati|23
2019-10-06|Atlanta|32|Houston|53
2019-10-06|Tampa Bay|24|New Orleans|31
2019-10-06|Minnesota|28|NY Giants|10
2019-10-06|Chicago|21|Oakland|24
2019-10-06|NY Jets|6|Philadelphia|31
2019-10-06|Baltimore|26|Pittsburgh|23
2019-10-06|Buffalo|14|Tennessee|7
2019-10-06|New England|33|Washington|7
2019-10-06|Denver|20|LA Chargers|13
2019-10-06|Indianapolis|19|Kansas City|13
2019-10-07|Cleveland|3|San Francisco|31
2019-10-10|NY Giants|14|New England|35
2019-10-13|Dallas|22|NY Jets|24
2019-10-13|Carolina|37|Tampa Bay|26
2019-10-13|Cincinnati|17|Baltimore|23
2019-10-13|Seattle|32|Cleveland|28
2019-10-13|New Orleans|13|Jacksonville|6
2019-10-13|Houston|31|Kansas City|24
2019-10-13|Washington|17|Miami|16
2019-10-13|Philadelphia|20|Minnesota|38
2019-10-13|Atlanta|33|Arizona|34
2019-10-13|San Francisco|20|LA Rams|7
2019-10-13|Tennessee|6|Denver|16
2019-10-13|Pittsburgh|24|LA Chargers|17
2019-10-14|Detroit|22|Green Bay|23
2019-10-17|Kansas City|30|Denver|6
2019-10-20|Philadelphia|10|Dallas|37
2019-10-20|LA Rams|37|Atlanta|10
2019-10-20|Miami|21|Buffalo|31
2019-10-20|Jacksonville|27|Cincinnati|17
2019-10-20|Minnesota|42|Detroit|30
2019-10-20|Oakland|24|Green Bay|42
2019-10-20|Houston|23|Indianapolis|30
2019-10-20|Arizona|27|NY Giants|21
2019-10-20|San Francisco|9|Washington|0
2019-10-20|LA Chargers|20|Tennessee|23
2019-10-20|New Orleans|36|Chicago|25
2019-10-20|Baltimore|30|Seattle|16
2019-10-21|New England|33|NY Jets|0
2019-10-24|Washington|9|Minnesota|19
2019-10-27|Seattle|27|Atlanta|20
2019-10-27|Philadelphia|31|Buffalo|13
2019-10-27|LA Chargers|17|Chicago|16
2019-10-27|NY Giants|26|Detroit|31
2019-10-27|NY Jets|15|Jacksonville|29
2019-10-27|Cincinnati|10|LA Rams|24
2019-10-27|Arizona|9|New Orleans|31
2019-10-27|Tampa Bay|23|Tennessee|27
2019-10-27|Denver|13|Indianapolis|15
2019-10-27|Carolina|13|San Francisco|51
2019-10-27|Oakland|24|Houston|27
2019-10-27|Cleveland|13|New England|27
2019-10-27|Green Bay|31|Kansas City|24
2019-10-28|Miami|14|Pittsburgh|27
2019-10-31|San Francisco|28|Arizona|25
2019-11-03|Houston|26|Jacksonville|3
2019-11-03|Washington|9|Buffalo|24
2019-11-03|Tennessee|20|Carolina|30
2019-11-03|Minnesota|23|Kansas City|26
2019-11-03|NY Jets|18|Miami|26
2019-11-03|Chicago|14|Philadelphia|22
2019-11-03|Indianapolis|24|Pittsburgh|26
2019-11-03|Green Bay|11|LA Chargers|26
2019-11-03|Detroit|24|Oakland|31
2019-11-03|Tampa Bay|34|Seattle|40
2019-11-03|Cleveland|19|Denver|24
2019-11-03|New England|20|Baltimore|37
2019-11-04|Dallas|37|NY Giants|18
2019-11-07|LA Chargers|24|Oakland|26
2019-11-10|Minnesota|28|Dallas|24
2019-11-10|Detroit|13|Chicago|20
2019-11-10|Baltimore|49|Cincinnati|13
2019-11-10|Buffalo|16|Cleveland|19
2019-11-10|Atlanta|26|New Orleans|9
2019-11-10|NY Giants|27|NY Jets|34
2019-11-10|Arizona|27|Tampa Bay|30
2019-11-10|Kansas City|32|Tennessee|35
2019-11-10|Miami|16|Indianapolis|12
2019-11-10|Carolina|16|Green Bay|24
2019-11-10|LA Rams|12|Pittsburgh|17
2019-11-11|Seattle|24|San Francisco|21
2019-11-14|Pittsburgh|7|Cleveland|21
2019-11-17|Dallas|35|Detroit|27
2019-11-17|Atlanta|29|Carolina|3
2019-11-17|Jacksonville|13|Indianapolis|33
2019-11-17|Buffalo|37|Miami|20
2019-11-17|Denver|23|Minnesota|27
2019-11-17|New Orleans|34|Tampa Bay|17
2019-11-17|NY Jets|34|Washington|17
2019-11-17|Houston|7|Baltimore|41
2019-11-17|Arizona|26|San Francisco|36
2019-11-17|Cincinnati|10|Oakland|17
2019-11-17|New England|17|Philadelphia|10
2019-11-17|Chicago|7|LA Rams|17
2019-11-18|Kansas City|24|LA Chargers|17
2019-11-21|Indianapolis|17|Houston|20
2019-11-24|Tampa Bay|35|Atlanta|22
2019-11-24|Denver|3|Buffalo|20
2019-11-24|NY Giants|14|Chicago|19
2019-11-24|Pittsburgh|16|Cincinnati|10
2019-11-24|Miami|24|Cleveland|41
2019-11-24|Carolina|31|New Orleans|34
2019-11-24|Oakland|3|NY Jets|34
2019-11-24|Detroit|16|Washington|19
2019-11-24|Seattle|17|Philadelphia|9
2019-11-24|Jacksonville|20|Tennessee|42
2019-11-24|Dallas|9|New England|13
2019-11-24|Green Bay|8|San Francisco|37
2019-11-25|Baltimore|45|LA Rams|6
2019-11-28|Buffalo|26|Dallas|15
2019-11-28|Chicago|24|Detroit|20
2019-11-28|New Orleans|26|Atlanta|18
2019-12-01|Washington|29|Carolina|21
2019-12-01|NY Jets|6|Cincinnati|22
2019-12-01|Tennessee|31|Indianapolis|17
2019-12-01|San Francisco|17|Baltimore|20
2019-12-01|Tampa Bay|28|Jacksonville|11
2019-12-01|Philadelphia|31|Miami|37
2019-12-01|Green Bay|31|NY Giants|13
2019-12-01|Cleveland|13|Pittsburgh|20
2019-12-01|LA Rams|34|Arizona|7
2019-12-01|Oakland|9|Kansas City|40
2019-12-01|LA Chargers|20|Denver|23
2019-12-01|New England|22|Houston|28
2019-12-02|Minnesota|30|Seattle|37
2019-12-05|Dallas|24|Chicago|31
2019-12-08|Carolina|20|Atlanta|40
2019-12-08|Baltimore|24|Buffalo|17
2019-12-08|Cincinnati|19|Cleveland|27
2019-12-08|Washington|15|Green Bay|20
2019-12-08|Denver|38|Houston|24
2019-12-08|Detroit|7|Minnesota|20
2019-12-08|San Francisco|48|New Orleans|46
2019-12-08|Miami|21|NY Jets|22
2019-12-08|Indianapolis|35|Tampa Bay|38
2019-12-08|LA Chargers|45|Jacksonville|10
2019-12-08|Pittsburgh|23|Arizona|17
2019-12-08|Kansas City|23|New England|16
2019-12-08|Tennessee|42|Oakland|21
2019-12-08|Seattle|12|LA Rams|28
2019-12-08|NY Giants|17|Philadelphia|23
2019-12-12|NY Jets|21|Baltimore|42
2019-12-15|LA Rams|21|Dallas|44
2019-12-15|Seattle|30|Carolina|24
2019-12-15|New England|34|Cincinnati|13
2019-12-15|Tampa Bay|38|Detroit|17
2019-12-15|Chicago|13|Green Bay|21
2019-12-15|Denver|3|Kansas City|23
2019-12-15|Miami|20|NY Giants|36
2019-12-15|Houston|24|Tennessee|21
2019-12-15|Philadelphia|37|Washington|27
2019-12-15|Cleveland|24|Arizona|38
2019-12-15|Jacksonville|20|Oakland|16
2019-12-15|Minnesota|39|LA Chargers|10
2019-12-15|Atlanta|29|San Francisco|22
2019-12-15|Buffalo|17|Pittsburgh|10
2019-12-16|Indianapolis|7|New Orleans|34
2019-12-21|Houston|23|Tampa Bay|20
2019-12-21|Buffalo|17|New England|24
2019-12-21|LA Rams|31|San Francisco|34
2019-12-22|Detroit|17|Denver|27
2019-12-22|Oakland|24|LA Chargers|17
2019-12-22|Jacksonville|12|Atlanta|24
2019-12-22|Baltimore|31|Cleveland|15
2019-12-22|Carolina|6|Indianapolis|38
2019-12-22|Cincinnati|35|Miami|38
2019-12-22|Pittsburgh|10|NY Jets|16
2019-12-22|New Orleans|38|Tennessee|28
2019-12-22|NY Giants|41|Washington|35
2019-12-22|Dallas|9|Philadelphia|17
2019-12-22|Arizona|27|Seattle|13
2019-12-22|Kansas City|26|Chicago|3
2019-12-23|Green Bay|23|Minnesota|10
2019-12-29|Washington|16|Dallas|47
2019-12-29|NY Jets|13|Buffalo|6
2019-12-29|New Orleans|42|Carolina|10
2019-12-29|Cleveland|23|Cincinnati|33
2019-12-29|Green Bay|23|Detroit|20
2019-12-29|LA Chargers|21|Kansas City|31
2019-12-29|Chicago|21|Minnesota|19
2019-12-29|Miami|27|New England|24
2019-12-29|Atlanta|28|Tampa Bay|22
2019-12-29|Pittsburgh|10|Baltimore|28
2019-12-29|Tennessee|35|Houston|14
2019-12-29|Indianapolis|20|Jacksonville|38
2019-12-29|Philadelphia|34|NY Giants|17
2019-12-29|Oakland|15|Denver|16
2019-12-29|Arizona|24|LA Rams|31
2019-12-29|San Francisco|26|Seattle|21
2020-01-04|Buffalo|19|Houston|22
2020-01-04|Tennessee|20|New England|13
2020-01-05|Minnesota|26|New Orleans|20
2020-01-05|Seattle|17|Philadelphia|9
2020-01-11|Minnesota|10|San Francisco|27
2020-01-11|Tennessee|28|Baltimore|12
2020-01-12|Houston|31|Kansas City|51
2020-01-12|Seattle|23|Green Bay|28
2020-01-19|Tennessee|24|Kansas City|35
2020-01-19|Green Bay|20|San Francisco|37
2020-02-02|San Francisco|20|Kansas City|31
