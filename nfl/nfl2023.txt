2023-09-07|Detroit|21|Kansas City|20
2023-09-10|Carolina|10|Atlanta|24
2023-09-10|Houston|9|Baltimore|25
2023-09-10|Cincinnati|3|Cleveland|24
2023-09-10|Jacksonville|31|Indianapolis|21
2023-09-10|Tampa Bay|20|Minnesota|17
2023-09-10|Tennessee|15|New Orleans|16
2023-09-10|San Francisco|30|Pittsburgh|7
2023-09-10|Arizona|16|Washington|20
2023-09-10|Green Bay|38|Chicago|20
2023-09-10|Las Vegas|17|Denver|16
2023-09-10|Miami|36|LA Chargers|34
2023-09-10|Philadelphia|25|New England|20
2023-09-10|LA Rams|30|Seattle|13
2023-09-10|Dallas|40|NY Giants|0
2023-09-11|Buffalo|16|NY Jets|22
2023-09-14|Minnesota|28|Philadelphia|34
2023-09-17|NY Jets|10|Dallas|30
2023-09-17|Green Bay|24|Atlanta|25
2023-09-17|Las Vegas|10|Buffalo|38
2023-09-17|Baltimore|27|Cincinnati|24
2023-09-17|Seattle|37|Detroit|31
2023-09-17|Indianapolis|31|Houston|20
2023-09-17|Kansas City|17|Jacksonville|3
2023-09-17|Chicago|17|Tampa Bay|27
2023-09-17|LA Chargers|24|Tennessee|27
2023-09-17|NY Giants|31|Arizona|28
2023-09-17|San Francisco|30|LA Rams|23
2023-09-17|Washington|35|Denver|33
2023-09-17|Miami|24|New England|17
2023-09-18|New Orleans|20|Carolina|17
2023-09-18|Cleveland|22|Pittsburgh|26
2023-09-21|NY Giants|12|San Francisco|30
2023-09-24|Dallas|16|Arizona|28
2023-09-24|Indianapolis|22|Baltimore|19
2023-09-24|Tennessee|3|Cleveland|27
2023-09-24|Atlanta|6|Detroit|20
2023-09-24|New Orleans|17|Green Bay|18
2023-09-24|Houston|37|Jacksonville|17
2023-09-24|Denver|20|Miami|70
2023-09-24|LA Chargers|28|Minnesota|24
2023-09-24|New England|15|NY Jets|10
2023-09-24|Buffalo|37|Washington|3
2023-09-24|Carolina|27|Seattle|37
2023-09-24|Chicago|10|Kansas City|41
2023-09-24|Pittsburgh|23|Las Vegas|18
2023-09-25|Philadelphia|25|Tampa Bay|11
2023-09-25|LA Rams|16|Cincinnati|19
2023-09-28|Detroit|34|Green Bay|20
2023-10-01|New England|3|Dallas|38
2023-10-01|Atlanta|7|Jacksonville|23
2023-10-01|Miami|20|Buffalo|48
2023-10-01|Minnesota|21|Carolina|13
2023-10-01|Denver|31|Chicago|28
2023-10-01|Baltimore|28|Cleveland|3
2023-10-01|Pittsburgh|6|Houston|30
2023-10-01|LA Rams|29|Indianapolis|23
2023-10-01|Tampa Bay|26|New Orleans|9
2023-10-01|Washington|31|Philadelphia|34
2023-10-01|Cincinnati|3|Tennessee|27
2023-10-01|Las Vegas|17|LA Chargers|24
2023-10-01|Arizona|16|San Francisco|35
2023-10-01|Kansas City|23|NY Jets|20
2023-10-02|Seattle|24|NY Giants|3
2023-10-05|Chicago|40|Washington|20
2023-10-08|Dallas|10|San Francisco|42
2023-10-08|Jacksonville|25|Buffalo|20
2023-10-08|Houston|19|Atlanta|21
2023-10-08|Carolina|24|Detroit|42
2023-10-08|Tennessee|16|Indianapolis|23
2023-10-08|NY Giants|16|Miami|31
2023-10-08|New Orleans|34|New England|0
2023-10-08|Baltimore|10|Pittsburgh|17
2023-10-08|Cincinnati|34|Arizona|20
2023-10-08|Philadelphia|23|LA Rams|14
2023-10-08|NY Jets|31|Denver|21
2023-10-08|Kansas City|27|Minnesota|20
2023-10-09|Green Bay|13|Las Vegas|17
2023-10-12|Denver|8|Kansas City|19
2023-10-15|Baltimore|24|Tennessee|16
2023-10-15|Washington|24|Atlanta|16
2023-10-15|Minnesota|19|Chicago|13
2023-10-15|Seattle|13|Cincinnati|17
2023-10-15|San Francisco|17|Cleveland|19
2023-10-15|New Orleans|13|Houston|20
2023-10-15|Indianapolis|20|Jacksonville|37
2023-10-15|Carolina|21|Miami|42
2023-10-15|New England|17|Las Vegas|21
2023-10-15|Detroit|20|Tampa Bay|6
2023-10-15|Arizona|9|LA Rams|26
2023-10-15|Philadelphia|14|NY Jets|20
2023-10-15|NY Giants|9|Buffalo|14
2023-10-16|Dallas|20|LA Chargers|17
2023-10-19|Jacksonville|31|New Orleans|24
2023-10-22|Detroit|6|Baltimore|38
2023-10-22|Las Vegas|12|Chicago|30
2023-10-22|Cleveland|39|Indianapolis|38
2023-10-22|Buffalo|25|New England|29
2023-10-22|Washington|7|NY Giants|14
2023-10-22|Atlanta|16|Tampa Bay|13
2023-10-22|Pittsburgh|24|LA Rams|17
2023-10-22|Arizona|10|Seattle|20
2023-10-22|Green Bay|17|Denver|19
2023-10-22|LA Chargers|17|Kansas City|31
2023-10-22|Miami|17|Philadelphia|31
2023-10-23|San Francisco|17|Minnesota|22
2023-10-26|Tampa Bay|18|Buffalo|24
2023-10-29|LA Rams|20|Dallas|43
2023-10-29|Houston|13|Carolina|15
2023-10-29|Minnesota|24|Green Bay|10
2023-10-29|New Orleans|38|Indianapolis|27
2023-10-29|New England|17|Miami|31
2023-10-29|NY Jets|13|NY Giants|10
2023-10-29|Jacksonville|20|Pittsburgh|10
2023-10-29|Atlanta|23|Tennessee|28
2023-10-29|Philadelphia|38|Washington|31
2023-10-29|Cleveland|20|Seattle|24
2023-10-29|Baltimore|31|Arizona|24
2023-10-29|Kansas City|9|Denver|24
2023-10-29|Cincinnati|31|San Francisco|17
2023-10-29|Chicago|13|LA Chargers|30
2023-10-30|Las Vegas|14|Detroit|26
2023-11-02|Tennessee|16|Pittsburgh|20
2023-11-05|Dallas|23|Philadelphia|28
2023-11-05|Miami|14|Kansas City|21
2023-11-05|Minnesota|31|Atlanta|28
2023-11-05|Seattle|3|Baltimore|37
2023-11-05|Arizona|0|Cleveland|27
2023-11-05|LA Rams|3|Green Bay|20
2023-11-05|Tampa Bay|37|Houston|39
2023-11-05|Washington|20|New England|17
2023-11-05|Chicago|17|New Orleans|24
2023-11-05|Indianapolis|27|Carolina|13
2023-11-05|NY Giants|6|Las Vegas|30
2023-11-05|Buffalo|18|Cincinnati|24
2023-11-06|LA Chargers|27|NY Jets|6
2023-11-09|Carolina|13|Chicago|16
2023-11-12|NY Giants|17|Dallas|49
2023-11-12|Indianapolis|10|New England|6
2023-11-12|Cleveland|33|Baltimore|31
2023-11-12|Houston|30|Cincinnati|27
2023-11-12|San Francisco|34|Jacksonville|3
2023-11-12|New Orleans|19|Minnesota|27
2023-11-12|Green Bay|19|Pittsburgh|23
2023-11-12|Tennessee|6|Tampa Bay|20
2023-11-12|Atlanta|23|Arizona|25
2023-11-12|Detroit|41|LA Chargers|38
2023-11-12|Washington|26|Seattle|29
2023-11-12|NY Jets|12|Las Vegas|16
2023-11-13|Denver|24|Buffalo|22
2023-11-16|Cincinnati|20|Baltimore|34
2023-11-19|Dallas|33|Carolina|10
2023-11-19|Pittsburgh|10|Cleveland|13
2023-11-19|Chicago|26|Detroit|31
2023-11-19|LA Chargers|20|Green Bay|23
2023-11-19|Arizona|16|Houston|21
2023-11-19|Tennessee|14|Jacksonville|34
2023-11-19|Las Vegas|13|Miami|20
2023-11-19|NY Giants|31|Washington|19
2023-11-19|Tampa Bay|14|San Francisco|27
2023-11-19|NY Jets|6|Buffalo|32
2023-11-19|Seattle|16|LA Rams|17
2023-11-19|Minnesota|20|Denver|21
2023-11-20|Philadelphia|21|Kansas City|17
2023-11-23|Green Bay|29|Detroit|22
2023-11-23|Washington|10|Dallas|45
2023-11-23|San Francisco|31|Seattle|13
2023-11-24|Miami|34|NY Jets|13
2023-11-26|New Orleans|15|Atlanta|24
2023-11-26|Pittsburgh|16|Cincinnati|10
2023-11-26|Jacksonville|24|Houston|21
2023-11-26|Tampa Bay|20|Indianapolis|27
2023-11-26|New England|7|NY Giants|10
2023-11-26|Carolina|10|Tennessee|17
2023-11-26|LA Rams|37|Arizona|14
2023-11-26|Cleveland|12|Denver|29
2023-11-26|Kansas City|31|Las Vegas|17
2023-11-26|Buffalo|34|Philadelphia|37
2023-11-26|Baltimore|20|LA Chargers|10
2023-11-27|Chicago|12|Minnesota|10
2023-11-30|Seattle|35|Dallas|41
2023-12-03|LA Chargers|6|New England|0
2023-12-03|Detroit|33|New Orleans|28
2023-12-03|Atlanta|13|NY Jets|8
2023-12-03|Arizona|24|Pittsburgh|10
2023-12-03|Indianapolis|31|Tennessee|28
2023-12-03|Miami|45|Washington|15
2023-12-03|Denver|17|Houston|22
2023-12-03|Carolina|18|Tampa Bay|21
2023-12-03|San Francisco|42|Philadelphia|19
2023-12-03|Cleveland|19|LA Rams|36
2023-12-03|Kansas City|19|Green Bay|27
2023-12-04|Cincinnati|34|Jacksonville|31
2023-12-07|New England|21|Pittsburgh|18
2023-12-10|Denver|24|LA Chargers|7
2023-12-10|Buffalo|20|Kansas City|17
2023-12-10|Seattle|16|San Francisco|28
2023-12-10|Minnesota|3|Las Vegas|0
2023-12-10|Houston|6|NY Jets|30
2023-12-10|Carolina|6|New Orleans|28
2023-12-10|Jacksonville|27|Cleveland|31
2023-12-10|Indianapolis|14|Cincinnati|34
2023-12-10|Detroit|13|Chicago|28
2023-12-10|LA Rams|31|Baltimore|37
2023-12-10|Tampa Bay|29|Atlanta|25
2023-12-10|Philadelphia|13|Dallas|33
2023-12-11|Green Bay|22|NY Giants|24
2023-12-11|Tennessee|28|Miami|27
2023-12-14|LA Chargers|21|Las Vegas|63
2023-12-16|Minnesota|24|Cincinnati|27
2023-12-16|Pittsburgh|13|Indianapolis|30
2023-12-16|Denver|17|Detroit|42
2023-12-17|Dallas|10|Buffalo|31
2023-12-17|Atlanta|7|Carolina|9
2023-12-17|Chicago|17|Cleveland|20
2023-12-17|Tampa Bay|34|Green Bay|20
2023-12-17|NY Jets|0|Miami|30
2023-12-17|NY Giants|6|New Orleans|24
2023-12-17|Houston|19|Tennessee|16
2023-12-17|Kansas City|27|New England|24
2023-12-17|San Francisco|45|Arizona|29
2023-12-17|Washington|20|LA Rams|28
2023-12-17|Baltimore|23|Jacksonville|7
2023-12-18|Philadelphia|17|Seattle|20
2023-12-21|New Orleans|22|LA Rams|30
2023-12-23|Cincinnati|11|Pittsburgh|34
2023-12-23|Buffalo|24|LA Chargers|22
2023-12-24|Dallas|20|Miami|22
2023-12-24|Indianapolis|10|Atlanta|29
2023-12-24|Green Bay|33|Carolina|30
2023-12-24|Cleveland|36|Houston|22
2023-12-24|Detroit|30|Minnesota|24
2023-12-24|Washington|28|NY Jets|30
2023-12-24|Seattle|20|Tennessee|17
2023-12-24|Jacksonville|12|Tampa Bay|30
2023-12-24|Arizona|16|Chicago|27
2023-12-24|New England|26|Denver|23
2023-12-25|Las Vegas|20|Kansas City|14
2023-12-25|NY Giants|25|Philadelphia|33
2023-12-25|Baltimore|33|San Francisco|19
2023-12-28|NY Jets|20|Cleveland|37
2023-12-30|Detroit|19|Dallas|20
2023-12-31|Miami|19|Baltimore|56
2023-12-31|New England|21|Buffalo|27
2023-12-31|Atlanta|17|Chicago|37
2023-12-31|Tennessee|3|Houston|26
2023-12-31|Las Vegas|20|Indianapolis|23
2023-12-31|Carolina|0|Jacksonville|26
2023-12-31|LA Rams|26|NY Giants|25
2023-12-31|Arizona|35|Philadelphia|31
2023-12-31|New Orleans|23|Tampa Bay|13
2023-12-31|San Francisco|27|Washington|10
2023-12-31|Pittsburgh|30|Seattle|23
2023-12-31|Cincinnati|17|Kansas City|25
2023-12-31|LA Chargers|9|Denver|16
2023-12-31|Green Bay|33|Minnesota|10
2024-01-06|Pittsburgh|17|Baltimore|10
2024-01-06|Houston|23|Indianapolis|19
2024-01-07|Dallas|38|Washington|10
2024-01-07|Tampa Bay|9|Carolina|0
2024-01-07|Cleveland|14|Cincinnati|31
2024-01-07|Minnesota|20|Detroit|30
2024-01-07|NY Jets|17|New England|3
2024-01-07|Atlanta|17|New Orleans|48
2024-01-07|Jacksonville|20|Tennessee|28
2024-01-07|Seattle|21|Arizona|20
2024-01-07|Chicago|9|Green Bay|17
2024-01-07|Denver|14|Las Vegas|27
2024-01-07|Kansas City|13|LA Chargers|12
2024-01-07|Philadelphia|10|NY Giants|27
2024-01-07|LA Rams|21|San Francisco|20
2024-01-07|Buffalo|21|Miami|14
2024-01-13|Cleveland|14|Houston|45
2024-01-13|Miami|7|Kansas City|26
2024-01-14|Green Bay|48|Dallas|32
2024-01-14|LA Rams|23|Detroit|24
2024-01-15|Pittsburgh|17|Buffalo|31
2024-01-15|Philadelphia|9|Tampa Bay|32
2024-01-20|Houston|10|Baltimore|34
2024-01-20|Green Bay|21|San Francisco|24
2024-01-21|Tampa Bay|23|Detroit|31
2024-01-21|Kansas City|27|Buffalo|24
2024-01-28|Kansas City|17|Baltimore|10
2024-01-28|Detroit|31|San Francisco|34
2024-02-11|San Francisco|22|Kansas City|25
